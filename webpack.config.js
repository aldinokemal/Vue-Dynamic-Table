const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, argv) => {
    console.log("mode: " + argv.mode);
    return {
        mode: 'production',
        // mode: 'development',
        entry: './script/main.js',
        output: {
            filename: argv.mode === 'production' ? 'bundle.[chunkhash].js' : 'bundle.js',
            path: path.resolve(__dirname, 'dist/js')
        },
        module: {
            rules: [{
                test: /\.vue$/,
                loader: 'vue-loader'
            },
                // this will apply to both plain `.js` files
                // AND `<script>` blocks in `.vue` files
                {
                    test: /\.js$/,
                    loader: 'babel-loader'
                },
                // this will apply to both plain `.css` files
                // AND `<style>` blocks in `.vue` files
                {
                    test: /\.css$/,
                    use: [
                        'vue-style-loader',
                        'css-loader'
                    ]
                }
            ]
        },
        plugins: [
            // make sure to include the plugin for the magic
            new VueLoaderPlugin(),
            new CleanWebpackPlugin(['dist']),
            new HtmlWebpackPlugin({
                title: 'Caching'
            }),
        ],
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.js'
            },
            extensions: ['*', '.js', '.vue', '.json']
        },
    }
};