# Dynamic Table Vue JS #
dymic table = tabel mengikuti jumlah range tanggal yang dipilih,
misal user input tanggal 25 mei 2018 - 27 mei 2018 maka width tabel akan mengikuti range tanggal

# [Demo](https://dynamic-table.hydrogendioxide.net/) #

## Langakah Pemakaian

1. Clone project ini
1. Masuk direktory dimana project ini di clone / di download
1. Pastikan install [NodeJS dan NPM](https://nodejs.org/en/download/) lalu lakukan ```npm install``` 
1. Buka vue.html via browser **(tidak perlu server)**

## Note
`npm run build` : untuk sekali compile <br />
`npm run watch` : untuk auto compile
___

## Dynamic Table : tanggal 14 - 16 -> 3 hari (14,15,16)
!["Perubahan Status Mahasiswa"](https://image.ibb.co/c7qit8/Screenshot_from_2018_05_27_20_53_37.png "Dynamic Table 1")

## Dynamic Table : tanggal 14 - 18 -> 5 hari (14,15,16,17,18)
!["Riwayat Pengajuan Surat"](https://image.ibb.co/nOLSLo/Screenshot_from_2018_05_27_20_53_57.png "Dynamic Table 2")
